FROM node:carbon
COPY package*.json ./
RUN npm install
RUN mkdir api
COPY api  ./api
EXPOSE 8080
CMD node ./api/log-service.js /data/logs 8080

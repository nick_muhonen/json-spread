#remove previous images

kubectl delete service log-service
kubectl delete deployment log-service
docker rmi log-service:std

#build docker images

docker build -t log-service:std -f ./docker/log-service.dockerfile .
kubectl create -f ./k8/log-service-deployment.yaml

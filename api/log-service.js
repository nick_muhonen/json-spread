const app = require('express')();
const bodyParser = require('body-parser');
const fs = require('fs');
const _ = require('lodash');
const fetch = require('node-fetch');

app.use(bodyParser.json()); // for parsing application/json

const {logDir, port} = (() => {
  const logDir = process.argv[2] || './logs';
  const port = process.argv[3] || '8080';

  console.log(`logs writing to ${logDir}`);

  return {
    logDir,
    port
  };
})();

app.put('/', (req, res) => {
  let fileName;
  let inc = 1;
  do {
    fileName = `${logDir}/log-${Date.now()}${inc > 1 ? `-${inc}` : ''}.json`
    inc+= 1;
  } while (fs.existsSync(fileName));

  fs.writeFile(fileName, JSON.stringify(req.body), () => {
    res.json({'success':'true'});
  });
});

app.get('/pod', (req, res) => {
  const files = fs.readdirSync(logDir);
  console.log(logDir);
  console.log(JSON.stringify(files, undefined, 2))
  const data = files
    .map(file => ({
      file,
      path: `${logDir}/${file}`}
    ))
    .map(fd => {
      const data = JSON.parse(fs.readFileSync(fd.path, 'utf8'));
      data.$meta = {
        file: fd.file,
        created: fs.statSync(fd.path).ctime,
      }

      return data;
    });
  res.json(data);
});

app.get('/combined', (req, res) => {
  const Api = require('kubernetes-client');
  const core = new Api.Core(Api.config.getInCluster());
  const ext = new Api.Extensions(Api.config.getInCluster());

  const dumpApi = obj => res.json(Object.keys(obj).reduce((result, key) => {
    result[key] = _.isFunction(obj[key]) ? 'func' : 'other';
    return result;
  }, {}));

  core.pods.getPromise()
    .then(result => {
      const fetches = result.items.filter(item => item.metadata.labels.app === 'log-service')
        .map(item => ({
          hostIP: item.status.hostIP,
          podIP: item.status.podIP
        }))
        .filter(item => item.podIP)
        .map(podIPs => {
          console.log(podIPs);
          return fetch(`http://${podIPs.podIP}:8080/pod`).then(response => response.json())
        });


      return Promise.all(fetches);
    }).then(results => {
      const items = results.reduce((result, arr) => result.concat(arr), []);
      const sorted = _.sortBy(items, item => item.$meta.created);
      res.json(sorted);
    }).catch(err => {
      res.json({error: err.message});
    });
});

app.listen(port);
